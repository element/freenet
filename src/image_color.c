#include <stdio.h>
#include <darknet.h>

/*
** 0 unknown
** 10 noir
** 1 blanc
** 2 rouge
** 3 orange
** 4 jaune
** 5 vert
** 6 bleu
** 7 Magenta (Violet)
** 8 darkgrey
** 9 lightgrey
*/

float max3_float(float n1, float n2, float n3)
{
  float max;

  if (n1 > n2)
    max = n1;
  else
    max = n2;
  if (n3 > max)
    max = n3;
  return (max);
}

float min3_float(float n1, float n2, float n3)
{
  float min;

  if (n1 < n2)
    min = n1;
  else
    min = n2;
  if (n3 < min)
    min = n3;
  return (min);
}


int hue_to_color(float hue)
{
  if (hue <= 30 || hue > 330)
    return (2); // Red
  else if (hue <= 90)
    return (3); // Yellow
  else if (hue <= 150)
    return (4); // Green
  else if (hue <= 210)
    return (5); // Cyan
  else if (hue <= 270)
    return (6); // Blue
  else
    return (7); // Magenta
}

int hsv_to_color(float h, float s, float v)
{
  if (v < 0.2)
    return (0);
  else if (v > 0.8)
    return (1);
  else
    return (hue_to_color(h));
}

void	print_color(int color)
{
  //if (color == 0)
  //printf("Unknown\n");
 if (color == 1)
  printf("White\n");
  else if (color == 2)
  printf("Red\n");
  else if (color == 3)
  printf("Yellow\n");
  else if (color == 4)
  printf("Green\n");
  else if (color == 5)
  printf("Cyan\n");
  else if (color == 6)
  printf("Blue\n");
  else if (color == 7)
  printf("Magenta\n");
  else if (color == 8)
  printf("Darkgrey\n");
  else if (color == 9)
  printf("Lightgrey\n");
  else if (color == 10)
  printf("Black\n");
}

void	rgb_hsv(float r, float g, float b, float *h, float *s, float *v)
// http://cs.rit.edu/~ncs/color/t_convert.html
{
  float min;
  float max;
  float delta;

  max = max3_float(r, g, b);
  min = min3_float(r, g, b);
  delta = max - min;
  *v = max;
  if (max != 0)
      *s = delta / max;
  else
    {
      *h = -1;
      *s = 0;
      *v = 0;
      return ;
    }
  if (r == max)
      *h = (g - b) / delta;
  else if (g == max)
      *h = 2 + (b - r) / delta;
  else
      *h = 4 + (r - g) / delta;
  *h *= 60;
  if (*h < 0)
      *h += 360;
}

float	channel_mean(float *data, int w, int h, int c)
{
  float	mean;

  mean = 0;
  for (int i = w * h * c; i < w * h * (c + 1); ++i)
    mean += data[i];
  mean /= w * h;
  return (mean);
}

void	write_channel(float *data, int w, int h, int c, float val)
{
  for (int i = w * h * c; i < w * h * (c + 1); ++i)
    data[i] = val;
}

int	is_seam_float(float n1, float n2, float n3, float step)
{
  if (n1 > n2 + step || n2 > n3 + step || n3 > n1 + step)
    return (0);
  return (1);
}

int greyscale(float r, float g, float b)
{
  if (is_seam_float(r, g, b, 0.15))
    {
      if (r + g + b < 1.5)
	return (8);
      else
	return (9);
    }
  return (-1);
}

int black_or_white(float r, float g, float b)
{
  if (r < 0.2 && g < 0.2 && b < 0.2)
    return (10);
  if (r > 0.8 && g > 0.8 && b > 0.8)
    return (1);
  return (-1);
}
/*
int main(int argc, char **argv)
{
  int color;
  float r, g, b;
  float h, s, v;
  image	im;
  image sized;
  char *filename;

  if (argc < 2)
    {
      printf("usage: %s [filename]\n", argv[0]);
      printf("Option:\t -mean\tgenerates mean image\n");
      return (-1);
    }
  filename = argv[1];
  printf("Mean Color of %s\n", filename);  
  im = load_image_color(filename, 0,0);

  printf("\nDimensions:\n");
  printf("width: %d pixels\n", im.w);
  printf("height: %d pixels\n", im.h);
  printf("channel: %d pixels\n", im.c);

  
  r = channel_mean(im.data, im.w, im.h, 0);
  g = channel_mean(im.data, im.w, im.h, 1);
  b = channel_mean(im.data, im.w, im.h, 2);

  printf("\nMean color (RGB):\n");
  printf("red = %f\n", r);
  printf("green = %f\n", g);
  printf("blue = %f\n", b);

  if (find_arg(argc, argv, "-mean"))
    {
      write_channel(im.data, im.w, im.h, 0, r);
      write_channel(im.data, im.w, im.h, 1, g);
      write_channel(im.data, im.w, im.h, 2, b);
      save_image(im, "mean");
    }

  rgb_hsv(r, g, b, &h, &s, &v);

  printf("\nMean color (HSV):\n");
  printf("hue = %f\n", h);
  printf("sat = %f\n", s);
  printf("val = %f\n", v);

  color = black_or_white(r, g, b);
  if (color == -1)
    color = greyscale(r, g, b);
  if (color == -1)
    color = hue_to_color(h);
  print_color(color);
  free_image(im);
  return (0);
}
*/
