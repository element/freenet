#! /bin/sh

if ! [ -f darknet ]
then
    echo "Can't found darknet"
    exit
fi
if ! [ -f yolov3.weights ]
then
    echo "Can't found yolov3.weights"
    exit
fi

./darknet detect cfg/yolov3.cfg yolov3.weights data/dog.jpg
